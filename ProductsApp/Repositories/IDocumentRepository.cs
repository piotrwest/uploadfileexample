﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductsApp.Repositories
{
    public interface IDocumentRepository
    {
        IEnumerable<Document> GetAll();
        Document Get(int id);
        Document Add(Document item);
        void Remove(int id);
        bool Update(Document item);
    }
}
