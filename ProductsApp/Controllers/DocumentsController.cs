﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProductsApp.Models;
using ProductsApp.Repositories;
using System.Net.Http.Headers;
using System.IO;

namespace ProductsApp.Controllers
{
    public class DocumentsController : ApiController
    {
        public static readonly IDocumentRepository Repository = new DocumentRepository();

        public IEnumerable<Document> GetAllProducts()
        {
            return Repository.GetAll();
        }

        public HttpResponseMessage GetDocument(int id)
        {
            var item = Repository.Get(id);
            if (item == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(new MemoryStream(item.Content));
            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = item.Name;

            return response;
        }

        public HttpResponseMessage PostProduct(Document item)
        {
            item = Repository.Add(item);
            var response = Request.CreateResponse<Document>(HttpStatusCode.Created, item);

            string uri = Url.Link("DefaultApi", new { id = item.Id });
            response.Headers.Location = new Uri(uri);
            return response;
        }
    }
}
