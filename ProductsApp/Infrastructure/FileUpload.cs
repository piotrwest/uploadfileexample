﻿using System.Net.Http;
using Newtonsoft.Json;

namespace ProductsApp.Infrastructure
{
    public class FileUpload<T> : IAmInitializableFileUpload
    {
        public T Value { get; set; }
        public string FileName { get; set; }
        public string MediaType { get; set; }
        public byte[] Buffer { get; set; }
        public HttpContent HttpFileContent { get; set; }

        void IAmInitializableFileUpload.Initialize(string filename, string rawModel, string mediaType, HttpContent httpFileContent)
        {
            Value = JsonConvert.DeserializeObject<T>(rawModel);
            MediaType = mediaType;
            FileName = filename.Replace("\"", "");
            HttpFileContent = httpFileContent;
        }
    }
}