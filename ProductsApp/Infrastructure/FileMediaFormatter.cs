﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace ProductsApp.Infrastructure
{
    public class FileMediaFormatter : MediaTypeFormatter
    {

        public FileMediaFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/octet-stream"));
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("multipart/form-data"));
        }

        public override bool CanReadType(Type type)
        {
            return IsSubclassOfRawGeneric(typeof(FileUpload<>), type);
        }

        //TODO: extract to some kind of utils?
        static bool IsSubclassOfRawGeneric(Type generic, Type toCheck)
        {
            while (toCheck != null && toCheck != typeof(object))
            {
                var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (generic == cur)
                {
                    return true;
                }
                toCheck = toCheck.BaseType;
            }
            return false;
        }

        public override bool CanWriteType(Type type)
        {
            return false;
        }

        public async override Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            if (!content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var parts = await content.ReadAsMultipartAsync();
            var files = parts.Contents.Where(f => f.Headers.ContentDisposition != null && !string.IsNullOrEmpty(f.Headers.ContentDisposition.FileName)).ToList();
            var formDatas = parts.Contents.Where(f => f.Headers.ContentDisposition != null && string.IsNullOrEmpty(f.Headers.ContentDisposition.FileName)).ToList();

            if (files.Count != 1)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            if (formDatas.Count != 1)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            var file = files.Single();
            var formData = formDatas.Single();

            var typeInActionDescription = type.GetGenericArguments()[0];
            var typeToInstantiate = typeof (FileUpload<>).MakeGenericType(typeInActionDescription);
            var result = (IAmInitializableFileUpload)Activator.CreateInstance(typeToInstantiate);

            var dataAsString = await formData.ReadAsStringAsync();
            var fileName = file.Headers.ContentDisposition.FileName;
            var mediaType = file.Headers.ContentType.MediaType;
            result.Initialize(fileName, dataAsString, mediaType, file);

            return result;
        }
    }
}