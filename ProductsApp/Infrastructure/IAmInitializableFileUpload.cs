﻿using System.Net.Http;

namespace ProductsApp.Infrastructure
{
    public interface IAmInitializableFileUpload
    {
        void Initialize(string filename, string rawModel, string mediaType, HttpContent httpFileContent);
    }
}