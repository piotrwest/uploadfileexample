﻿'use strict';

app.factory('ProductSvc', function ($resource, PRODUCT_API_URL) {
    return $resource(PRODUCT_API_URL + ':id', {},
        {
            'get': { isArray: true }
        });
});