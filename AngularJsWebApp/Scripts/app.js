﻿var app = angular.module('angJsWebApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'angularFileUpload'
]);

app.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: './Views/main.html',
            controller: 'MainCtrl'
        })
        .when('/addProduct', {
            templateUrl: './Views/AddProduct.html',
            controller: 'AddProductCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
});

app.constant('PRODUCT_API_URL', 'http://localhost/ProductsApp/api/products/');
app.constant('DOCUMENT_API_URL', 'http://localhost/ProductsApp/api/documents/');