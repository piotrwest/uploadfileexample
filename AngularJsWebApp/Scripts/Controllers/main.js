﻿
app.controller('MainCtrl', function ($scope, ProductSvc, $log, PRODUCT_API_URL, $location, DOCUMENT_API_URL) {
        var products = ProductSvc.get(null, function(response) {
            angular.forEach(response, function(value, key) {
                value.Link = DOCUMENT_API_URL + value.DocumentId;
                value.ShowLink = value.DocumentId !== 0;
            });
        });

        $scope.products = products;

        $scope.addNewProduct = function() {
            $location.path('/addProduct');
        };
    }
);