﻿
app.controller('AddProductCtrl', function($scope, ProductSvc, $log, $location, $upload, PRODUCT_API_URL) {
        $scope.product = {
            Name: 'Change me',
            Category: 'Utils',
            Price: '1'
        };

        $scope.cancel = function() {
            $location.path('/');
        };

        $scope.addProduct = function() {
            if (!$scope.selectedFile) {
                alert('select file first');
                return;
            }

            $scope.upload = $upload.upload({
                url: PRODUCT_API_URL, //upload.php script, node.js route, or servlet url
                // method: 'POST' or 'PUT',
                //headers: {'Content-Type': 'header-value'},
                // withCredentials: true,
                data: { item: $scope.product },
                file: $scope.selectedFile, // or list of files: $files for html5 only
                /* set the file formData name ('Content-Desposition'). Default is 'file' */
                //fileFormDataName: myFile, //or a list of names for multiple files (html5).
                /* customize how data is added to formData. See #40#issuecomment-28612000 for sample code */
                //formDataAppender: function(formData, key, val){}
            }).progress(function (evt) {
                //console.log(angular.toJson(evt, true));
                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).success(function(data, status, headers, config) {
                // file is uploaded successfully
                console.log(data);
                $scope.finishAdding(data);
            }).error($scope.finishAddingFail);
            //.then(success, error, progress); 
            //.xhr(function(xhr){xhr.upload.addEventListener(...)})// access and attach any event listener to XMLHttpRequest.

            //Without file upload
            //ProductSvc.save($scope.product).$promise
            //    .then($scope.finishAdding) //TODO: Can we do this better? Why we have to add a function to $scope?
            //    .catch($scope.finishAddingFail);
        };

        $scope.finishAdding = function(data) {
            $log.log(angular.toJson(data, true));
            $location.path('/');
        };

        $scope.finishAddingFail = function(error) {
            $log.error(angular.toJson(error, true));
            alert(angular.toJson(error, true));
        };

        $scope.onFileSelect = function($files) {
            $log.log(angular.toJson($files, true));
            $scope.selectedFile = $files[0];
            $log.log(angular.toJson($scope.selectedFile, true));
        };
    }
);