﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AngularJsWebApp._Default" %>

<!doctype html>
<html ng-app="angJsWebApp">
<head runat="server">
    <script src="./Content/Scripts/angular/angular-file-upload-shim.js"></script>
    <script src="./Content/Scripts/angular/angular.js"></script>
    <script src="./Content/Scripts/angular/angular-file-upload.js"></script>
    <script src="./Content/Scripts/angular/angular-cookies.js"></script>
    <script src="./Content/Scripts/angular/angular-resource.js"></script>
    <script src="./Content/Scripts/angular/angular-sanitize.js"></script>
    <script src="./Content/Scripts/angular/angular-route.js"></script>
    <script src="./Scripts/app.js"></script>
    <script src="./Scripts/Services/ProductSvc.js"></script>
    <script src="./Scripts/Controllers/main.js"></script>
    <script src="./Scripts/Controllers/AddProductCtrl.js"></script>
    <link rel="stylesheet" href="./Styles/main.css">
</head>
<body>
    <div class="container" ng-view=""></div>
</body>
</html>


